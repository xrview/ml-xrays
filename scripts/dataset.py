import os
import matplotlib.pyplot as plt
import numpy as np

from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.datasets import ImageFolder
from torchvision.utils import make_grid
import torch

from torch.utils.data.sampler import WeightedRandomSampler

# from torchsampler import ImbalancedDatasetSampler

# Set the directory for the data
data_dir = '../filtered/'
# data_dir = os.path.join(os.path.dirname(os.getcwd()), 'filtered') 

def load_class_names(path=os.path.join(data_dir, 'class_names.txt')):
    # Class names
    class_names = open(path).readlines()
    class_names = [folder.split('\n')[0] for folder in class_names]

    return class_names

def make_weights_for_balanced_classes(images, nclasses):                        
    count = [0] * nclasses

    for val in images:                                              
        count[val[1]] += 1

    weight_per_class = [0.] * nclasses                                
    N = float(sum(count))

    for i in range(nclasses):                                               
        weight_per_class[i] = N / float(count[i])
    
    weight = [0] * len(images)

    for idx, val in enumerate(images):                                 
        weight[idx] = weight_per_class[val[1]]

    return weight, weight_per_class

def prepare_loader(config):

    # Make transforms and use data loaders
    # We'll use these a lot, so make them variables
    mean_nums = [0.485, 0.456, 0.406]
    std_nums = [0.229, 0.224, 0.225]

    img_size = config['imgsize']
    data_transforms = {
        'train': transforms.Compose([
                    transforms.RandomHorizontalFlip(),
                    # transforms.RandomResizedCrop(size=256),
                    # transforms.RandomRotation(degrees=15),
                    # transforms.ColorJitter(),
                    transforms.Resize(size=256),
                    transforms.CenterCrop(size=img_size),  # Image net standards
                    transforms.ToTensor(),
                    transforms.Normalize(mean_nums, std_nums)]), 
        'test': transforms.Compose([
                    transforms.Resize(size=256),
                    transforms.CenterCrop(size=img_size),
                    transforms.ToTensor(),
                    transforms.Normalize(mean_nums, std_nums)]),
    }

    print("Initializing Datasets and Dataloaders...")

    # Use the image folder function to create datasets
    chosen_datasets = {x: ImageFolder(os.path.join(data_dir, x), data_transforms[x])
                        for x in ['train', 'test']}

    # # For unbalanced dataset we create a weighted sampler                       
    # weights = {x: make_weights_for_balanced_classes(chosen_datasets[x].imgs, len(chosen_datasets[x].classes))[0]
    #             for x in ['train', 'test']}  

    # weights = {x: torch.DoubleTensor(weights[x])
    #             for x in ['train', 'test']}
            
    weights_per_class = {x: make_weights_for_balanced_classes(chosen_datasets[x].imgs, len(chosen_datasets[x].classes))[1]
                            for x in ['train', 'test']}
    weights_per_class = {x: torch.FloatTensor(weights_per_class[x])
                            for x in ['train', 'test']}

    # sampler = {x: WeightedRandomSampler(weights[x], len(weights[x]))
    #             for x in ['train', 'test']}

    # Make iterables with the dataloaders
    dataloaders_dict = {x: DataLoader(chosen_datasets[x], 
                                    batch_size=config['batch_size'], 
                                    shuffle=x=='train',
                                    # sampler=sampler[x],
                                    # sampler=ImbalancedDatasetSampler(chosen_datasets[x]),
                                    num_workers=12)
                        for x in ['train', 'test']}

    return dataloaders_dict['train'], dataloaders_dict['test'], weights_per_class